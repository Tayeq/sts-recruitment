class Beers {
    beersUrl = '/api/beers/';
    brewersUrl = '/api/brewers/';
    countriesUrl = '/api/countries/';
    typesUrl = '/api/types/';
    beerUrl = '/api/beer/';
    perPage = 10;

    beers = null;
    brewers = null;
    countries = null;
    types = null;
    parameters = [];

    init = function () {
        this.getBeers();
        this.getAllBrewers();
        this.getAllCountries();
        this.getAllTypes();
        this.bindEvents();
    }

    setBeersUrl = function (url) {
        this.beersUrl = url;
    }

    getBeersUrl = function (page = null) {
        var me = this;
        var url = (page !== null) ? this.beersUrl + '/' + page + '/' + '?' : this.beersUrl + '?';

        $(me.parameters).each(function (key, parameter) {
            url += parameter.key + '=' + parameter.value + '&';
        })

        return url;
    }

    getBeers = function () {
        var me = this;
        $.ajax({
            url: me.getBeersUrl()
        }).done(function (beers) {
            me.beers = beers;
            me.loadBeers();
        });
    }

    // getAllBeers = function () {
    //     var me = this;
    //     $.ajax({
    //         url: me.beersUrl
    //     }).done(function (beers) {
    //         me.allBeers = beers;
    //         me.beers = beers;
    //         me.loadBeers();
    //     });
    // }

    getAllBrewers = function () {
        var me = this;
        $.ajax({
            url: me.brewersUrl
        }).done(function (brewers) {
            me.brewers = brewers;
            me.loadBrewers();
        });
    }

    getAllCountries = function () {
        var me = this;
        $.ajax({
            url: me.countriesUrl
        }).done(function (countries) {
            me.countries = countries;
            me.loadCountries();
        });
    }

    getAllTypes = function () {
        var me = this;
        $.ajax({
            url: me.typesUrl
        }).done(function (types) {
            me.types = types;
            me.loadTypes();
        });
    }

    loadBeers = function () {
        var me = this;
        $("#beers tbody").html('');
        beers = me.beers;

        $(beers).each(function (key, beer) {
            var row = '';
            row += '<tr class="beer" data-id="' + beer.id + '">';
            row += '<td>' + beer.id + '</td>';
            row += '<td>' + beer.name + '</td>';
            row += '<td>' + beer.brewer + '</td>';
            row += '<td>' + beer.type + '</td>';
            row += '<td>' + beer.country + '</td>';
            row += '<td>' + beer.price + '</td>';
            row += '</tr>';
            $("#beers tbody").append(row);
        });

        //me.renderPagination();
    }

    loadBrewers = function () {
        var me = this;
        $("#brewersSelect").prop("disabled", false);
        $("#brewersSelect").html('<option value="0">All Brewers</option>');
        $(me.brewers).each(function (key, brewer) {
            var option = '';
            option += '<option value="' + brewer.id + '">';
            option += brewer.name;
            option += '</option>';
            $("#brewersSelect").append(option);
        });
    }

    loadCountries = function () {
        var me = this;
        $("#countriesSelect").prop("disabled", false);
        $("#countriesSelect").html('<option value="0">All Countries</option>');
        $(me.countries).each(function (key, country) {
            var option = '';
            option += '<option value="' + country.id + '">';
            option += country.name;
            option += '</option>';
            $("#countriesSelect").append(option);
        });
    }

    loadTypes = function () {
        var me = this;
        $("#typesSelect").prop("disabled", false);
        $("#typesSelect").html('<option value="0">All Types</option>');
        $(me.types).each(function (key, type) {
            var option = '';
            option += '<option value="' + type.id + '">';
            option += type.name;
            option += '</option>';
            $("#typesSelect").append(option);
        });
    }

    showLoading = function () {
        $("#beers tbody").html('<tr><td class="text-center" colspan="6">Loading...</td></tr>');
    }

    refreshBeers = function () {
        this.showLoading();
        this.getBeers(this.getBeersUrl());
    }

    setParameter = function (key, value) {
        var me = this;
        var parameter = {
            'key': key,
            'value': value
        }

        var parameterExist = false;
        $(me.parameters).each(function (parameterKey, parameterValue) {
            if (parameterValue.key === key) {
                if (value === '' || (['brewer_id', 'country_id', 'type_id'].indexOf(parameterValue.key) !== -1 && value == 0)) {
                    me.parameters = me.parameters.filter(item => item !== parameterValue);
                } else {
                    me.parameters[parameterKey] = parameter;
                }

                parameterExist = true;
            }
        })

        if (!parameterExist) {
            this.parameters.push(parameter);
        }

        return this.parameters;
    }

    bindEvents = function () {
        var me = this;

        $("#nameInput").on('change', function () {
            me.setParameter('name', this.value);
            me.refreshBeers();
        });

        $("#brewersSelect").on('change', function () {
            var selected = this.value;
            me.setParameter('brewer_id', selected);
            me.refreshBeers();
        });

        $("#priceFromInput").on('change', function () {
            me.setParameter('price_from', this.value);
            me.refreshBeers();
        });

        $("#priceToInput").on('change', function () {
            me.setParameter('price_to', this.value);
            me.refreshBeers();
        });

        $("#countriesSelect").on('change', function () {
            var selected = this.value;
            me.setParameter('country_id', selected);
            me.refreshBeers();
        });

        $("#typesSelect").on('change', function () {
            var selected = this.value;
            me.setParameter('type_id', selected);
            me.refreshBeers();
        });

        $(document).on('click', 'tr:not("[class*=beer-description]")', function () {
            var clickedElement = this;
            var beerId = $(this).data('id');

            if ($("#beers tbody").children('tr.beer-description').data('id') !== beerId) {

                $('.beer-description').remove()

                $.ajax({
                    url: me.beerUrl + beerId
                }).done(function (beer) {
                    var content = '';
                    content += '<tr class="beer-description text-center" data-id="' + beer.id + '" >';
                    content += '<td colspan="6">';
                    content += '<a href="#" class="remove-description float-right">x</a>';
                    content += '<img src="' + beer.image + '"/>';
                    content += '<p>Name: ' + beer.name + '</p>';
                    content += '<p>Brewer: ' + beer.brewer + '</p>';
                    content += '<p>Type: ' + beer.type + '</p>';
                    content += '<p>Country: ' + beer.country + '</p>';
                    content += '<p>Price: ' + beer.price + '</p>';
                    content += '<p>Size: ' + beer.size + '</p>';
                    content += '<p>Category: ' + beer.category + '</p>';
                    content += '<p>Abv: ' + beer.abv + '</p>';
                    content += '<p>Style: ' + beer.style + '</p>';
                    content += '<p>Attributes: ' + beer.attributes + '</p>';
                    content += '<p>On sale: ' + (beer.on_sale ? 'Yes' : 'No') + '</p>';
                    content += '</td>';
                    content += '</tr>';
                    $(content).insertAfter(clickedElement);
                });
            }
        });

        $(document).on('click', '.remove-description', function (e) {
            e.preventDefault();
            $('.beer-description').remove();
        });

    }


    // renderPagination = function (active = 1) {
        // var me = this;
        // var pages = [];
        // var page = 1;
        // var count = 0;
        //
        // pages.push(page);
        // page++;
        //
        // for (var i = 0; i < me.beers.length; i++) {
        //     count++;
        //     if(count === me.perPage){
        //         count = 0;
        //         pages.push(page);
        //         page++;
        //     }
        // }
        // $('#beersPagination').html('');
        // $(pages).each(function(key, value){
        //     if(value === active){
        //         $('#beersPagination').append('<li class="page-item active"><a class="page-link" href="#">'+value+'</a></li>');
        //     }else{
        //         $('#beersPagination').append('<li class="page-item"><a class="page-link" href="#">'+value+'</a></li>');
        //     }
        //
        // });
    // }
}