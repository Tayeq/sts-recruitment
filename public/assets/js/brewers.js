class Brewers {
    beersUrl = '/beers/';
    brewersUrl = '/api/brewers?order=desc&orderBy=beerCount';

    brewers = null;
    parameters = [];

    init = function () {
        this.getAllBrewers();
        this.bindEvents();
    }

    getAllBrewers = function () {
        var me = this;
        $.ajax({
            url: me.brewersUrl
        }).done(function (brewers) {
            me.brewers = brewers;
            me.loadBrewers();
        });
    }

    loadBrewers = function () {
        var me = this;
        $("#brewers tbody").html('');

        $(me.brewers).each(function (key, brewer) {
            var row = '';
            row += '<tr class="brewer" data-id="' + brewer.id + '">';
            row += '<td>' + brewer.id + '</td>';
            row += '<td>' + brewer.name + '</td>';
            row += '<td>' + brewer.beerCount + '</td>';
            row += '<td class="text-right"><a class="btn btn-primary btn-sm" href="'+me.beersUrl+'?brewer_id='+brewer.id+'">Show beers</a></td>';
            row += '</tr>';
            $("#brewers tbody").append(row);
        });
    }


    bindEvents = function () {
        var me = this;

    }
}