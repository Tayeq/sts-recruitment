# Install
## Installation

Prepare .env file from env.example

Use [composer](https://getcomposer.org/) to install application.

```
composer install
```
Configure database in **env**.

```
DATABASE_URL="mysql://root:@127.0.0.1:3306/database"
```

Create and update database
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```

If you have [symfony server](https://symfony.com/doc/current/setup/symfony_server.html) installed just run:
```
symfony server:start

```

# Usage
## Beer update
```
/beers/update
```
This command will import beers from [http://ontariobeerapi.ca/](http://ontariobeerapi.ca/)

## Api GET Calls

```
/api/beers
/api/beers/{page}             ## Example: /api/beers/1
/api/beers?filter=value       ## Example: /api/beers?price_from=1.30&price_to=40
/api/beer{id}                 ## Example: /api/beer/1
/api/brewers
/api/types
/api/countries

```
### Available beers filters:
- name
- brewer_id
- brewer_name
- price_from
- price_to
- country_id
- country_code
- country_name
- type_id
- type_name
---
- offset
- limit
