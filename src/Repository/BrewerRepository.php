<?php

namespace App\Repository;

use App\Entity\Brewer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Brewer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Brewer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Brewer[]    findAll()
 * @method Brewer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrewerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Brewer::class);
    }

    public function findOneOrCreate($name): Brewer
    {
        $brewer = $this->findOneBy(['name' => $name]);

        if ($brewer === null)
        {
            $brewer = new Brewer();
            $brewer->setName($name);
            $this->_em->persist($brewer);
            $this->_em->flush();
        }

        return $brewer;
    }

    public function findAllWithBeerCount($orderBy = 'name', $order = 'ASC'){
        $queryBuilder = $this->_em
            ->createQueryBuilder()
            ->addSelect('brewer.id as id')
            ->addSelect('brewer.name as name')
            ->addSelect('COUNT(beers) as beerCount')
            ->from('App:Brewer', 'brewer')
            ->innerJoin('brewer.beers', 'beers')
            ->groupBy('brewer.id')
            ->orderBy($orderBy, $order)
        ;

        return $queryBuilder->getQuery()->getResult();

    }
}
