<?php

namespace App\Repository;

use App\Entity\Beer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Beer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Beer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Beer[]    findAll()
 * @method Beer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BeerRepository extends ServiceEntityRepository
{
    private $brewerRepository;
    private $typeRepository;
    private $countryRepository;

    public function __construct(ManagerRegistry $registry, BrewerRepository $brewerRepository, TypeRepository $typeRepository, CountryRepository $countryRepository)
    {
        parent::__construct($registry, Beer::class);

        $this->brewerRepository = $brewerRepository;
        $this->typeRepository = $typeRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * @return Beer Returns an Beer Object
     */

    public function findOneOrCreate(array $beer): Beer
    {
        $country = $this->countryRepository->findOneOrCreate($beer['country']);
        $type = $this->typeRepository->findOneOrCreate($beer['type']);
        $brewer = $this->brewerRepository->findOneOrCreate($beer['brewer']);

        if ($newBeer = $this->findOneBy([
            'name' => $beer['name'],
            'country' => $country,
            'type' => $type,
            'brewer' => $brewer,
            'price' => $beer['price'],
        ])) {
            return $newBeer;
        }

        $newBeer = new Beer();
        $newBeer->setName($beer['name']);
        $newBeer->setCountry($country);
        $newBeer->setType($type);
        $newBeer->setBrewer($brewer);

        $newBeer->setPrice($beer['price']);
        $newBeer->setSize($beer['size']);
        $newBeer->setImage($beer['image_url']);
        $newBeer->setCategory($beer['category']);
        $newBeer->setAbv($beer['abv']);
        $newBeer->setStyle($beer['style']);
        $newBeer->setAttributes($beer['attributes']);
        $newBeer->setOnSale($beer['on_sale']);

        $this->_em->persist($newBeer);
        $this->_em->flush();

        return $newBeer;
    }

    /**
     * @param array $parameter
     * @return array Returns an Beer Objects array
     */
    public function findByParameter(array $parameters, array $order = [], $limit = null, $offset = null): array
    {

        $queryBuilder = $this->_em
            ->createQueryBuilder()
            ->select('beer')
            ->from('App:Beer', 'beer')
            ->leftJoin('beer.country', 'country')
            ->leftJoin('beer.brewer', 'brewer')
            ->leftJoin('beer.type', 'type');

        isset($parameters['name']) ? $queryBuilder->andWhere('beer.name LIKE :name')->setParameter('name', "%{$parameters['name']}%") : null;
        isset($parameters['brewer_id']) ? $queryBuilder->andWhere('brewer.id = :brewerId')->setParameter('brewerId', $parameters['brewer_id']) : null;
        isset($parameters['brewer_name']) ? $queryBuilder->andWhere('brewer.name = :brewerName')->setParameter('brewerName', $parameters['brewer_name']) : null;
        isset($parameters['price_from']) ? $queryBuilder->andWhere('beer.price >= :priceFrom')->setParameter('priceFrom', $parameters['price_from']) : null;
        isset($parameters['price_to']) ? $queryBuilder->andWhere('beer.price <= :priceTo')->setParameter('priceTo', $parameters['price_to']) : null;
        isset($parameters['country_id']) ? $queryBuilder->andWhere('country.id = :countryId')->setParameter('countryId', $parameters['country_id']) : null;
        isset($parameters['country_code']) ? $queryBuilder->andWhere('country.code = :countryCode')->setParameter('countryCode', $parameters['country_code']) : null;
        isset($parameters['country_name']) ? $queryBuilder->andWhere('country.name = :countryName')->setParameter('countryName', $parameters['country_name']) : null;
        isset($parameters['type_id']) ? $queryBuilder->andWhere('type.id = :typeId')->setParameter('typeId', $parameters['type_id']) : null;
        isset($parameters['type_name']) ? $queryBuilder->andWhere('type.name = :typeName')->setParameter('typeName', $parameters['type_name']) : null;

        $queryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $queryBuilder->getQuery()->getResult();
    }
}
