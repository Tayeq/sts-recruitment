<?php

namespace App\Controller;

use App\Repository\BrewerRepository;
use App\Repository\CountryRepository;
use App\Repository\TypeRepository;
use App\Service\BeerUpdater;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BeerRepository;
use Symfony\Component\HttpFoundation\Request;

class ApiBeerController extends AbstractController
{
    private $entityManager;
    private $beerRepository;
    private $brewerRepository;
    private $typeRepository;
    private $countryRepository;

    public function __construct(EntityManagerInterface $entityManager, BeerRepository $beerRepository, BrewerRepository $brewerRepository, TypeRepository $typeRepository, CountryRepository $countryRepository)
    {
        $this->entityManager = $entityManager;
        $this->beerRepository = $beerRepository;
        $this->brewerRepository = $brewerRepository;
        $this->typeRepository = $typeRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * @Route("/api/beers/{page}", name="api_beers")
     * @return JsonResponse
     */
    public function beers(Request $request, int $page = 0)
    {
        $limit = null;
        $offset = null;
        $return = [];
        $parameters = $request->query->all();

        if ($page > 0) {
            $limit = $request->query->get('limit') ? $request->query->get('limit') : 10;
            $offset = $request->query->get('offset') ? $request->query->get('offset') : ($page - 1) * $limit;
        }

        $orderBy = $request->query->get('orderBy') ? $request->query->get('orderBy') : 'name';
        $order = $request->query->get('order') ? $request->query->get('order') : 'ASC';

        $beers = $this->beerRepository->findByParameter(
            $parameters,
            array($orderBy => $order),
            $limit,
            $offset
        );

        foreach ($beers as $beer) {
            $return[] = $beer->toArray();
        }

        return $this->json($return);
    }

    /**
     * @Route("/api/beer/{id}", name="api_beer", requirements={"id"="\d+"})
     * @return JsonResponse
     */
    public function beer(int $id)
    {
        $beer = $this->beerRepository->findOneBy([
            'id' => $id
        ]);

        if ($beer) {
            return $this->json($beer->toArray());
        }

        return $this->json('Beer with id ' . $id . ' not found.');
    }

    /**
     * @Route("/api/brewers", name="api_brewers")
     * @return JsonResponse
     */
    public function brewers(Request $request)
    {
        $return = [];

        $orderBy = in_array($request->query->get('orderBy'), ['id', 'name', 'beerCount']) ? $request->query->get('orderBy') : 'brewer.name';
        $order = in_array(strtoupper($request->query->get('order')), ['ASC', 'DESC']) ? $request->query->get('order') : 'ASC';

        $brewers = $this->brewerRepository->findAllWithBeerCount($orderBy, $order);

        return $this->json($brewers);
    }

    /**
     * @Route("/api/beers/update", name="api_beers_update")
     */
    public function update(BeerUpdater $beerUpdater)
    {
        $beerUpdater->update();

        return $this->json([
            'message' => 'Beers updated sucessfully!',
        ]);
    }

    /**
     * @Route("/api/types", name="api_types")
     * @return JsonResponse
     */
    public function types()
    {
        $return = [];
        $types = $this->typeRepository->findBy([],['name' => 'ASC']);

        foreach ($types as $type) {
            $return[] = $type->toArray();
        }

        return $this->json($return);
    }

    /**
     * @Route("/api/countries", name="api_countries")
     * @return JsonResponse
     */
    public function countries()
    {
        $return = [];
        $countries = $this->countryRepository->findBy([],['name' => 'ASC']);

        foreach ($countries as $country) {
            $return[] = $country->toArray();
        }

        return $this->json($return);
    }
}
