<?php

namespace App\Controller;

use App\Repository\BeerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\BeerUpdater;

class BeerController extends AbstractController
{
    private $beerRepository;

    public function __construct(BeerRepository $beerRepository)
    {
        $this->beerRepository = $beerRepository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->redirect('beers');
    }

    /**
     * @Route("/beers", name="beers")
     */
    public function beers()
    {
        return $this->render('beers/beers.html.twig');
    }

    /**
     * @Route("/brewers", name="brewers")
     */
    public function brewers()
    {
        return $this->render('beers/brewers.html.twig');
    }

    /**
     * @Route("/beers/update", name="beer_update")
     */
    public function update(BeerUpdater $beerUpdater)
    {
        $beerUpdater->update();

        return $this->render('beers/updated.html.twig');
    }

    /**
     * @Route("/beers/datatables/getAll", name="beer_get_all_datatables")
     */
    public function getForDatatables(){
        $beers = $this->beerRepository->findAll();
        $return = [];
        foreach($beers as $beer){
            $return[$beer->getId()]= [
                $beer->getId(),
                $beer->getName(),
                $beer->getBrewer()->getName(),
                $beer->getType()->getName(),
                $beer->getCountry()->getName(),
                $beer->getPrice(),
            ];
        }
        return $this->json(['data' => array_values($return)]);
    }
}
