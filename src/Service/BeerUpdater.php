<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use App\Repository\BeerRepository;


class BeerUpdater
{
    private $beerApiUrl = 'http://ontariobeerapi.ca/beers/';
    private $beerRequiredFields = ['name', 'price', 'brewer', 'type', 'country'];

    private $beers;

    private $client;
    private $beerRepository;

    public function __construct(BeerRepository $beerRepository)
    {
        $this->client = HttpClient::create();
        $this->beerRepository = $beerRepository;

        $this->getApiBeers();
    }

    public function update():void
    {
        foreach ($this->beers as $beer) {
            if($this->checkBeerTemplate($beer, false)){
                $this->beerRepository->findOneOrCreate((array) $beer);
            }
        }
    }

    private function getBeerApiUrl(): string
    {
        return $this->beerApiUrl;
    }

    private function updateBeerApiUrl($apiUrl = false): string
    {
        if ($apiUrl) {
            $this->beerApiUrl = $apiUrl;
        }

        return $this->beerApiUrl;
    }

    private function getApiBeers(): array
    {
        $response = $this->client->request('GET', $this->beerApiUrl);

        if ($response->getHeaders()['content-type'][0] !== 'application/json') {
            throw new \Exception('API returned bad content type.');
        }

        $beers = json_decode($response->getContent());

        if (!count($beers) > 0) {
            throw new \Exception('No beers returned by API.');
        }

        $this->checkBeerTemplate($beers[0]);

        return $this->beers = $beers;
    }

    private function checkBeerTemplate($beer, $throwError = true): bool
    {
        $missingFields = [];
        $beer = (array)$beer;

        foreach ($this->beerRequiredFields as $requiredField) {
            if (!array_key_exists($requiredField, ($beer))) {
                $missingFields[] = $requiredField;
            }
        }

        if (count($missingFields) > 0) {
            if($throwError){
                throw new \Exception('Returned bad beer format. Missing fields: (' . implode($missingFields, " | ") . ')');
            }
           return false;
        }
        return true;
    }

}